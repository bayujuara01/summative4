package com.ariefyantobayu.Summative4.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.ariefyantobayu.Summative4.entity.Product;
import com.ariefyantobayu.Summative4.repository.ProductRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class ProductControllerTest {

	@MockBean
	ProductRepository productRepository;

	@Autowired
	MockMvc mockMvc;

	@Test
	@DisplayName("Load Context Test")
	void loadContextTest() throws Exception {
		assertThat(productRepository).isNotNull();
	}

	@Test
	@DisplayName("Success Get All Product And Have Products")
	void testSuccessGetAllProductAndProductHaveLookingFor() throws Exception {
		when(productRepository.findAll()).thenReturn(
				List.of(new Product(1L, "TEST_PRODUCT-1", "TEST-1", 10L, 10_000L, LocalDateTime.now(), null),
						new Product(2L, "TEST_PRODUCT-2", "TEST-2", 40L, 20_000L, LocalDateTime.now(), null),
						new Product(3L, "TEST_PRODUCT-3", "TEST-3", 50L, 30_000L, LocalDateTime.now(), null)));

		mockMvc.perform(get("/products")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(content().string(containsString("name")));
	}

	@Test
	@DisplayName("Failed Get Product, Empty Product")
	void testFailedGetProductWhereProductEmpty() throws Exception {
		when(productRepository.findAll()).thenReturn(List.of());
		mockMvc.perform(get("/products")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
				.andExpect(content().string(containsString("[]")));
	}

	@Test
	@DisplayName("Failed Search Product, No Request Body")
	void testSearchAllProductByQueryParam_WithNoRequestBodyFailed() throws Exception {
		when(productRepository.findAllByNameOrCategory(anyString(), anyString())).thenReturn(List.of());
		String requestBodyJson = new ObjectMapper().writeValueAsString(new Product());
		mockMvc.perform(post("/products/search").contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE).content(requestBodyJson)).andDo(print())
				.andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Not Found")));

		mockMvc.perform(post("/products/search").contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE).content(requestBodyJson)).andDo(print())
				.andExpect(status().is5xxServerError()).andExpect(content().string(containsString("Not Found")));
	}

	@Test
	@DisplayName("Success Search Product, With Stock Only")
	void testSearchAllProductByQueryParam_WithRequestBodyStockOnlySuccess() throws Exception {
		String someExpectedObjectJson = new ObjectMapper().writeValueAsString(
				new Product(1L, "TEST_OK_PRODUCT-1", "TEST-1", 10L, 10_000L, LocalDateTime.now(), null));
		when(productRepository.findAllByNameOrCategoryWithStock(anyString(), anyString(), anyLong())).thenReturn(
				List.of(new Product(1L, "TEST_OK_PRODUCT-1", "TEST-1", 10L, 10_000L, LocalDateTime.now(), null),
						new Product(2L, "TEST_OK_PRODUCT-2", "TEST-2", 40L, 20_000L, LocalDateTime.now(), null),
						new Product(3L, "TEST_PRODUCT-3", "TEST-3", 50L, 30_000L, LocalDateTime.now(), null)));
		String requestBodyJson = new ObjectMapper().writeValueAsString(new Product("TEST_OK", null, null, null, null));
		mockMvc.perform(post("/products/search").contentType(MediaType.APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON_VALUE).content(requestBodyJson)).andDo(print())
				.andExpect(status().isOk()).andExpect(content().string(containsString("TEST_OK_PRODUCT-2")))
				.andExpect(content().json(someExpectedObjectJson));
	}

	@Test
	@DisplayName("Success Search Product, With Product Name Only")
	void testSearchAllProductByQueryParam_WithRequestBodyProductNameOnlySuccess() throws Exception {
		when(productRepository.findAllByNameOrCategoryWithStock(anyString(), anyString(), anyLong())).thenReturn(
				List.of(new Product(1L, "TEST_PRODUCT-1", "TEST-1", 10L, 10_000L, LocalDateTime.now(), null)));
		String requestBodyJson = "{\"name\":\"PRODUCT\"}";
				
		mockMvc.perform(post("/products/search").content(requestBodyJson)).andDo(print())
				.andExpect(status().isOk()).andExpect(content().string(containsString("name")))
				.andExpect(status().isOk()).andExpect(content().string(containsString("TEST_PRODUCT-1")));
	}

	@Test
	@DisplayName("Success Search Product, With Category AND Username")
	void testSearchAllProductByQueryParam_WithRequestBodyUsernameAndCategorySuccess() throws Exception {
		when(productRepository.findAllByNameOrCategoryWithStock(anyString(), anyString(), anyLong())).thenReturn(
				List.of(new Product(1L, "TEST_PRODUCT-1", "TEST-1", 10L, 10_000L, LocalDateTime.now(), null),
						new Product(1L, "TEST_PRODUCT-2", "TEST-2", 10L, 10_000L, LocalDateTime.now(), null)));
		String requestBodyJson = new ObjectMapper().writeValueAsString(new Product("PRODUCT", null, null, null, null));
		mockMvc.perform(post("/products/search").contentType(MediaType.APPLICATION_JSON_VALUE).content(requestBodyJson))
				.andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("name")))
				.andExpect(status().isOk()).andExpect(content().string(containsString("TEST_PRODUCT-1")))
				.andExpect(status().isOk()).andExpect(content().string(containsString("TEST-2")));
	}

}
