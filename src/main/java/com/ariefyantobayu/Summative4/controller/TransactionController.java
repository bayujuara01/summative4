package com.ariefyantobayu.Summative4.controller;

import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ariefyantobayu.Summative4.entity.Product;
import com.ariefyantobayu.Summative4.entity.Transaction;
import com.ariefyantobayu.Summative4.handler.ResponseHandler;
import com.ariefyantobayu.Summative4.repository.ProductRepository;
import com.ariefyantobayu.Summative4.repository.TransactionRepository;
import com.ariefyantobayu.Summative4.response.DataResponse;
import com.ariefyantobayu.Summative4.response.ErrorResponse;

@RestController
public class TransactionController {
	public static DateTimeFormatter sqlDateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	@Autowired
	ProductRepository productRepository;

	@Autowired
	TransactionRepository transactionRepository;

	@GetMapping(value = "/payments", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> getAllProduct() {
		List<Transaction> requestTransaction = transactionRepository.findAll();
		ResponseHandler response;

		if (requestTransaction.size() > 0) {
			response = DataResponse.of(HttpStatus.OK, requestTransaction);
		} else {
			response = DataResponse.of(HttpStatus.OK, List.of(), "Don't have transaction looking for");
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

//	@PostMapping(value = "/payments/search", produces = MediaType.)
//	ResponseEntity<ResponseHandler> getAllProduct() {
//		return null;
//	}

	@PostMapping(value = "/payments", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> makeTransactionProduct(@RequestBody Transaction transaction) {

		if (transaction.getProductId() != null && !Objects.isNull(transaction.getQty())) {
			Optional<Product> productRequest = productRepository.findById(transaction.getProductId());

			if (productRequest.isPresent() && productRequest.get().getStock() >= transaction.getQty()) {
				Product product = productRequest.get();
				Long productPrice = product.getPrice();
				product.decrementStock(transaction.getQty());
				Product updatedProduct = productRepository.save(product);
				transaction.setProduct(updatedProduct);
				transaction.setTotalPrice(productPrice * transaction.getQty());
				Transaction savedTransaction = transactionRepository.save(transaction);

				System.out.println("Qty OK");
				return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, savedTransaction));
			} else {
				return ResponseEntity.internalServerError().body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR,
						"Not Enough stock", "Stock not enough, please increment your product quantity"));
			}

		} else {
			return ResponseEntity.internalServerError().body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR,
					"No Argument Error", "Please provide productId and qty"));
		}
	}

	@PostMapping("/payments/search")
	ResponseEntity<ResponseHandler> getAllProductByDate(@RequestBody(required = false) HashMap<String, String> dateMap) {
		if (Objects.nonNull(dateMap) && dateMap.containsKey("date")) {

			List<Transaction> transactionRequest = transactionRepository
					.findAllBetweenDate(dateMap.get("date"));
			ResponseHandler response;

			if (transactionRequest.size() > 0) {
				response = DataResponse.of(HttpStatus.OK, transactionRequest);
			} else {
				response = DataResponse.of(HttpStatus.OK, List.of(), "Don't have transaction looking for");

			}

			return ResponseEntity.status(HttpStatus.OK).body(response);
		} else {
			return ResponseEntity.internalServerError()
					.body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, "No Argument Error", "Please insert json body with 'date' property"));
		}
	}
}
