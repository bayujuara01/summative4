package com.ariefyantobayu.Summative4.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ariefyantobayu.Summative4.entity.Product;
import com.ariefyantobayu.Summative4.handler.ResponseHandler;
import com.ariefyantobayu.Summative4.repository.ProductRepository;
import com.ariefyantobayu.Summative4.repository.TransactionRepository;
import com.ariefyantobayu.Summative4.response.DataResponse;
import com.ariefyantobayu.Summative4.response.ErrorResponse;

@RestController
public class ProductController {

	@Autowired
	ProductRepository productRepository;

	@Autowired
	TransactionRepository transactionRepository;

	@GetMapping(value = "/products", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> getAllProduct() {
		List<Product> requestProducts = productRepository.findAll();
		ResponseHandler response;

		if (requestProducts.size() > 0) {
			response = DataResponse.of(HttpStatus.OK, requestProducts);
		} else {
			response = DataResponse.of(HttpStatus.OK, List.of(), "Don't have product looking for");
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@PostMapping(value = "/products/search", 
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> SearchAllProductByQueryParam(@RequestBody Optional<Product> product) {
		List<Product> productResult = null;
		ResponseHandler response;
		Product productRequest = product.get();
		String productNameRequest = productRequest.getName() != null ? productRequest.getName() : "";
		String productCategoryRequest = productRequest.getCategory() != null ? productRequest.getCategory() : ""; 

		if (product.isPresent() && productRequest.getStock() != null) {
			productResult = productRepository.findAllByNameOrCategoryWithStock(productNameRequest,
					productCategoryRequest, productRequest.getStock());
		} else if (product.isPresent() && (!productNameRequest.isBlank() || !productCategoryRequest.isBlank())) {
			productResult = productRepository.findAllByNameOrCategory(productNameRequest, productCategoryRequest);
		} else {
			return ResponseEntity.internalServerError().body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR,
					"Request Body Not Found", "Please provide request body (name, category or stock"));
		}

		if (productResult.size() > 0) {
			response = DataResponse.of(HttpStatus.OK, productResult);
		} else {
			response = DataResponse.of(HttpStatus.OK, List.of(), "Don't have product looking for");
		}

		return ResponseEntity.status(HttpStatus.OK).body(response);
	}

	@PostMapping(value = "/products", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> saveBatchProduct(@RequestBody List<Product> products) {
		boolean isHaveZeroStock = false;

		for (Product product : products) {
			if (product.getStock() <= 0) {
				isHaveZeroStock = true;
				break;
			}
		}

		if (isHaveZeroStock) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ErrorResponse.of(
					HttpStatus.INTERNAL_SERVER_ERROR, "Stock must greather than 0", "Provide stock greather than -"));
		} else {
			List<Product> savedProducts = productRepository.saveAll(products);
			return ResponseEntity.ok(DataResponse.of(HttpStatus.CREATED, savedProducts));
		}
	}

	@DeleteMapping(value = "/products/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> deleteProductById(@PathVariable(name = "id") Long productId) {
		Optional<Product> productRequest = productRepository.findById(productId);

		if (productRequest.isPresent()) {
			int removedTransaction = transactionRepository.deleteByProductId(productId);
			productRepository.deleteById(productId);
			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, productRequest.get()));
		} else {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(
					ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR, "Error Delete Product", "Product id not found"));
		}

	}

	@PutMapping(value = "/products/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	ResponseEntity<ResponseHandler> updateProductById(@PathVariable(name = "id") Long productId,
			@RequestBody Product product) {
		Optional<Product> productRequest = productRepository.findById(productId);
		if (productRequest.isPresent()) {
			product.setId(productId);
			Product productUpdated = productRepository.save(product);

			return ResponseEntity.ok(DataResponse.of(HttpStatus.OK, productUpdated));
		} else {
			return ResponseEntity.internalServerError().body(ErrorResponse.of(HttpStatus.INTERNAL_SERVER_ERROR,
					"Error Update Product", "Error update product, id not valid"));
		}
	}

}
