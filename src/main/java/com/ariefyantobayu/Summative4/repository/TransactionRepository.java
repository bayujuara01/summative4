package com.ariefyantobayu.Summative4.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.ariefyantobayu.Summative4.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {
	
	@Transactional
	@Modifying
	@Query(value = "DELETE FROM transaction WHERE product_id = ?1", nativeQuery = true)
	int deleteByProductId(Long id);
	
	@Query(value = "SELECT * FROM transaction WHERE DATE(create_at) = DATE(?1) ",
			nativeQuery = true)
	List<Transaction> findAllBetweenDate(String date);
}
