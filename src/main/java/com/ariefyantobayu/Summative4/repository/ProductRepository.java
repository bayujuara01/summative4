package com.ariefyantobayu.Summative4.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ariefyantobayu.Summative4.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

	@Query("SELECT p FROM Product p WHERE p.name LIKE %:name% AND p.category LIKE %:category%")
	List<Product> findAllByNameOrCategory(@Param("name") String name, @Param("category") String category);

	@Query("SELECT p FROM Product p WHERE p.name LIKE %:name% AND p.category LIKE %:category% AND p.stock <= :stock")
	List<Product> findAllByNameOrCategoryWithStock(@Param("name") String name, @Param("category") String category,
			@Param("stock") Long stock);
}
