package com.ariefyantobayu.Summative4.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "product")
@DynamicUpdate
public class Product {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "product_name")
	private String name;
	@Column(name = "category")
	private String category;
	@Column(name = "stock")
	private Long stock;
	@Column(name = "price")
	private Long price;
	@Column(name = "create_at")
	@CreationTimestamp
	private LocalDateTime createAt;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "product")
	@JsonProperty(access = Access.WRITE_ONLY)
	private List<Transaction> transactions;
	
	public Product() {}
	
	public Product(String name, String category, Long stock, Long price, LocalDateTime createAt) {
		super();
		this.name = name;
		this.category = category;
		this.stock = stock;
		this.price = price;
		this.createAt = createAt;
	}

	public Product(Long id, String name, String category, Long stock, Long price, LocalDateTime createAt,
			List<Transaction> transactions) {
		super();
		this.id = id;
		this.name = name;
		this.category = category;
		this.stock = stock;
		this.price = price;
		this.createAt = createAt;
		this.transactions = transactions;
	}
	

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public void setStock(Long stock) {
		this.stock = stock;
	}
	
	public void decrementStock(Long qty) {
		this.stock -= qty;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public void setCreateAt(LocalDateTime createAt) {
		this.createAt = createAt;
	}

	public Long getId() {
		return id;
	}
	

	public String getName() {
		return name;
	}

	public String getCategory() {
		return category;
	}

	public Long getStock() {
		return stock;
	}
	
	
	public Long getPrice() {
		return price;
	}

	public LocalDateTime getCreateAt() {
		return createAt;
	}
	
    public List<Transaction> getTransactions() {
        return transactions;
    }

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", category=" + category + ", stock=" + stock + ", price="
				+ price + ", createAt=" + createAt + "]";
	}
    
    
}
