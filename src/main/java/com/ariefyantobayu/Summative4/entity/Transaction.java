package com.ariefyantobayu.Summative4.entity;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name = "transaction")
public class Transaction {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "qty")
	private Long qty;
	@Column(name = "total_price")
	private Long totalPrice;
	@Column(name = "create_at")
	@CreationTimestamp
	private LocalDateTime createAt;
	@ManyToOne(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
	@JoinColumn(name = "product_id")
	private Product product;
	
	@Transient
	@JsonProperty(access = Access.WRITE_ONLY)
	private Long productId;
	
	public Transaction() {}
	
	public Transaction(Long qty, Long totalPrice, LocalDateTime createAt, Product product) {
		super();
		this.qty = qty;
		this.totalPrice = totalPrice;
		this.createAt = createAt;
		this.product = product;
	}
	
	public Transaction(Long id, Long qty, Long totalPrice, LocalDateTime createAt, Product product) {
		super();
		this.id = id;
		this.qty = qty;
		this.totalPrice = totalPrice;
		this.createAt = createAt;
		this.product = product;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Long getId() {
		return id;
	}

	public Long getQty() {
		return qty;
	}
	
	public void setQty(Long qty) {
		this.qty = qty;
	}

	public Long getTotalPrice() {
		return totalPrice;
	}
	
	public void setTotalPrice(Long totalPrice) {
		this.totalPrice = totalPrice;
	}

	public LocalDateTime getCreateAt() {
		return createAt;
	}
	
	public Long getProductId() {
		return productId;
	}
	
	public void setProductId(Long productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", qty=" + qty + ", totalPrice=" + totalPrice + ", createAt=" + createAt + "]";
	}

	
	
}
