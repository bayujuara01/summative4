package com.ariefyantobayu.Summative4.response;

import java.util.List;

import org.springframework.http.HttpStatus;

import com.ariefyantobayu.Summative4.handler.ResponseHandler;

public class ErrorResponse implements ResponseHandler {
	private int status;
	private String message;
	private List<String> errors;
	
	public int getStatus() {
		return status;
	}

	public List<String> getErrors() {
		return errors;
	}

	public String getMessage() {
		return message;
	}
	
	private ErrorResponse(int status, String message, List<String> errors) {
		super();
		this.status = status;
		this.errors = errors;
		this.message = message;
	}
	
	public static ErrorResponse of(HttpStatus httpStatus, List<String> errors) {
		return new ErrorResponse(httpStatus.value(), "Don't have message attached", errors);
	}
	
	public static ErrorResponse of(HttpStatus httpStatus, List<String> errors, String message) {
		return new ErrorResponse(httpStatus.value(), message, errors);
	}
	
	public static ErrorResponse of(HttpStatus httpStatus, String error, String message) {
		return new ErrorResponse(httpStatus.value(), message, List.of(error));	
	}
}
