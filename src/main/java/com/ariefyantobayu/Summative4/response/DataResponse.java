package com.ariefyantobayu.Summative4.response;

import org.springframework.http.HttpStatus;

import com.ariefyantobayu.Summative4.handler.ResponseHandler;

public class DataResponse implements ResponseHandler {
	private int status;
	private Object data;
	private String message;
	
	public int getStatus() {
		return status;
	}

	public Object getData() {
		return data;
	}
	
	public String getMessage() {
		return message;
	}

	private DataResponse(int status, Object data, String message) {
		this.status = status;
		this.data = data;
		this.message = message;
	}
	
	public static DataResponse of(HttpStatus httpStatus, Object data) {
		return new DataResponse(httpStatus.value(), data, "-");
	}
	
	public static DataResponse of(HttpStatus httpStatus, Object data, String message) {
		return new DataResponse(httpStatus.value(), data, message);
	}
}
