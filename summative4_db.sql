CREATE DATABASE IF NOT EXISTS nexmart;
USE nexmart;

INSERT INTO product(product_name, category, stock, create_at) VALUES 
("Tempura 500G", "ILM", 50, CURRENT_TIMESTAMP()),
("Bintang 500G", "ILM", 40, CURRENT_TIMESTAMP()),
("Minaku Bola Udang 250G", "MINAKU", 15, CURRENT_TIMESTAMP()),
("Minaku Scallop 500G", "MINAKU", 10, CURRENT_TIMESTAMP()),
("Sosis Ngetop 30", "SOSIS", 50, CURRENT_TIMESTAMP());

SELECT * FROM product;

INSERT INTO transaction(product_id, qty, total_price, create_at) VALUES 
(1, 10, 100000, CURRENT_TIMESTAMP()),
(1, 5, 50000, CURRENT_TIMESTAMP()),
(2, 20, 200000, CURRENT_TIMESTAMP()),
(3, 2, 30000, CURRENT_TIMESTAMP()),
(4, 2, 40000, CURRENT_TIMESTAMP()),
(5, 10, 250000, CURRENT_TIMESTAMP());

SELECT * FROM  product WHERE product_name LIKE '%%' AND category LIKE '%%' AND stock <= 40;

SELECT * FROM transaction WHERE create_at BETWEEN '2021-10-26' AND '2021-10-30'; 